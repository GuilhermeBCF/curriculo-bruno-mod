# BRUNO BUENO DA SILVA

---
# DADOS PESSOAIS:
CPF: 402.214.534.90

EMAIL: bruno.uni@gmail.com

TEL: (14) 99888-6799

ENDEREÇO: Geracina mendes romildo n 55

---
# FORMAÇÃO ACADÊMICA:
- Analise e Desenvolvimento de Sistemas - Unimar - 2023 até 2025
- Técnico em Eletroeletrônica - SENAI
- Ensino Médio - SESI
- Analista de suporte Técnico - Micropro
- Digitação - Micropro

---
# EXPERIÊNCIA:

- Analista de Suporte - SUPERMERCADOS KAWAKAMI - MARÍLIA
- 2018 - ATUALMENTE
---
# HABILIDADES:

- Boa comunicação

- Autonomia

- Resiliência

---
# IDIOMAS:
- Inglês intermediário
- Espanhol intermediário